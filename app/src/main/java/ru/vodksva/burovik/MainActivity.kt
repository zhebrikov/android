package ru.vodksva.burovik

import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKit
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.map.*
import com.yandex.mapkit.map.Map
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.image.ImageProvider


class MainActivity : UserLocationObjectListener, AppCompatActivity() {
    private val PERMISSIONS_REQUEST_FINE_LOCATION = 1
    private lateinit var mapview: MapView
    private lateinit var userLocationLayer: UserLocationLayer;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey("6fdf8d72-bb96-49a3-8235-df862c7197c0")
        MapKitFactory.initialize(this)

        setContentView(R.layout.activity_main)

        mapview = findViewById(R.id.mapview)

        mapview.map.addCameraListener { map, cameraPosition, cameraUpdateReason, b ->
            Log.d("test", map.cameraPosition.target.latitude.toString())
        }

        mapview
            .map
            .mapObjects
            .addPlacemark(Point(57.412749,  42.108529))
            .addTapListener(mapObjectTapListener)


        mapview.map.move(
            CameraPosition(Point(57.412749,  42.108529), 15.0f, 0.0f, 0.0f),
            Animation(Animation.Type.SMOOTH, 5F),
            null
        )


        requestLocationPermission()

        val mapKit: MapKit = MapKitFactory.getInstance()

        mapKit.resetLocationManagerToDefault();

        userLocationLayer = mapKit.createUserLocationLayer(mapview.mapWindow)
        userLocationLayer.isVisible = true
        userLocationLayer.isHeadingEnabled = true

        userLocationLayer.setObjectListener(this)
    }

    private val mapObjectTapListener =
        MapObjectTapListener { mapObject, point ->
            var pt = point.toString();
            showDialogCustom(point.latitude.dec().toString())
            true
        }

    private fun showDialogCustom(text: String){
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle(R.string.dialogTitle)
        //set message for alert dialog
        builder.setMessage(text)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes"){dialogInterface, which ->
            Toast.makeText(applicationContext,"clicked yes", Toast.LENGTH_LONG).show()
        }
        //performing cancel action
        builder.setNeutralButton("Cancel"){dialogInterface , which ->
            Toast.makeText(applicationContext,"clicked cancel\n operation cancel",Toast.LENGTH_LONG).show()
        }
        //performing negative action
        builder.setNegativeButton("No"){dialogInterface, which ->
            Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun requestLocationPermission(){
        if(checkSelfPermission(
                this,
                "android.permission.ACCESS_FINE_LOCATION"
        ) !== PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this,
                arrayOf("android.permission.ACCESS_FINE_LOCATION"),
                PERMISSIONS_REQUEST_FINE_LOCATION
            )
        }
    }

    override fun onStop() {
        mapview.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        mapview.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onObjectAdded(userLocationView: UserLocationView) {
        userLocationLayer.setAnchor(
            PointF((mapview.width() * 0.5).toFloat(), (mapview.height() * 0.5).toFloat()),
            PointF((mapview.width() * 0.5).toFloat(), (mapview.height() * 0.83).toFloat())
        )

        userLocationView.arrow.setIcon(
            ImageProvider.fromResource(
                this, R.drawable.ic_launcher_foreground
            )
        )

        val pinIcon: CompositeIcon = userLocationView.pin.useCompositeIcon()
        pinIcon.setIcon(
            "icon",
            ImageProvider.fromResource(this, R.drawable.ic_launcher_foreground),
            IconStyle().setAnchor(PointF(0f, 0f))
                .setRotationType(RotationType.NO_ROTATION).setZIndex(0f).setScale(1f)
        )

        pinIcon.setIcon(
            "pin",
            ImageProvider.fromResource(this, R.drawable.ic_launcher_foreground),
            IconStyle().setAnchor(PointF(0.5f, 0.5f)).setRotationType(RotationType.ROTATE).setZIndex(1f)
                .setScale(0.5f)
        )

        userLocationView.accuracyCircle.fillColor = Color.BLUE and -0x66000001
    }

    override fun onObjectRemoved(p0: UserLocationView) {
    }

    override fun onObjectUpdated(p0: UserLocationView, p1: ObjectEvent) {
    }
}

